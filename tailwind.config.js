/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./**/*.html'],
  theme: {
    screens:{
      '2xl': {'max': '2559px'},
      'xl': {'max': '1439px'},
      'lg': {'max': '1023px'},
      'md': {'max': '767px'},
      'sm': {'max': '479px'},
    },
    extend: {
      backgroundImage: {
        'tan' : "url('https://i.hizliresim.com/fjenki4.png')",
        'search' : "url('https://i.hizliresim.com/488kupi.png')"
       }
    },
  },
  plugins: [],
}
